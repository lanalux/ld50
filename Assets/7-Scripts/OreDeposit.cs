using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreDeposit : MonoBehaviour
{
    [SerializeField] List<Renderer> oreMesh = new List<Renderer>();
    [SerializeField] List<Collider> oreCol = new List<Collider>();
    [SerializeField] List<Renderer> crystalMesh = new List<Renderer>();
    [SerializeField] Collider damageCol;
    [SerializeField] float currentHealth;
    [SerializeField] float maxHealth = 100.0f;
    [SerializeField] float timeToRegen = 20.0f;
    [SerializeField] List<GemPickup> crystals = new List<GemPickup>();

    void Start(){
        currentHealth=maxHealth;
    }

    public void TakeDamage(float damage){
        currentHealth -= damage;
        float healthRatio = currentHealth/maxHealth;
        int currentOreMesh = 0;
        if (healthRatio <= 1f && healthRatio > 0.9f){
           currentOreMesh = 0;
        } else if(healthRatio <= 0.9f && healthRatio > 0.8f){
            currentOreMesh = 1;
        } else if(healthRatio <= 0.8f && healthRatio > 0.6f){
            currentOreMesh = 2;
        } else if(healthRatio <= 0.6f && healthRatio > 0.4f){
            currentOreMesh = 3;
        } else if(healthRatio <= 0.4f && healthRatio > 0.2f){
            currentOreMesh = 4;
        } else if(healthRatio <= 0.2f && healthRatio > 0.0f){
            currentOreMesh = 5;
        } else {
            currentOreMesh = 6;
        }
        if(currentOreMesh==oreMesh.Count){
            damageCol.enabled=false;
            // StartCoroutine(Regen());
            OnAxeCollide.Instance.PlayDepositDoneSound();
            crystals[0].TurnOn();
            
            for (int i=0; i<oreMesh.Count; i++){
                oreMesh[i].enabled=false;
                oreCol[i].enabled=false;
            }
        } else {
            for (int i=0; i<oreMesh.Count; i++){
                if(i==currentOreMesh){
                    oreMesh[i].enabled=true;
                    oreCol[i].enabled=true;
                } else {
                    oreMesh[i].enabled=false;
                    oreCol[i].enabled=false;
                }
            }
        }
        
        
        
    }

    void TurnBackOn(){
        GemControls.Instance.PlayRespawnSound();
        currentHealth=maxHealth;
        oreMesh[0].enabled=true;
        oreCol[0].enabled=true;
        damageCol.enabled=true;
    }

    public IEnumerator Regen(){
        yield return new WaitForSeconds(timeToRegen);
        TurnBackOn();
    }

}
