using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDControls : MonoBehaviour
{
    public static HUDControls Instance {get;set;}
   [SerializeField] Text gemCountText;

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
    }
    public void UpdateHUD(){
        gemCountText.text = GemControls.Instance.gemCount.ToString();
    }
}
