using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorControls : MonoBehaviour
{
    public static CursorControls Instance {get;set;}
   [SerializeField] GameObject cursorDot, left, right, r;
   [SerializeField] Text cursorText;

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
    }

    public void ShowText(string message){
        cursorText.text = message;
        cursorDot.SetActive(true);
        left.SetActive(true);
    }
    public void DotOnly(){
        cursorText.text = "";
        cursorDot.SetActive(true);
        left.SetActive(false);
    }
    public void HideCursor(){
        cursorText.text = "";
        cursorDot.SetActive(false);
        left.SetActive(false);
    }

    public void ShowPlacingInfo(){
        right.SetActive(true);
        r.SetActive(true);
    }
    
    public void HidePlacingInfo(){
        right.SetActive(false);
        r.SetActive(false);
    }
}
