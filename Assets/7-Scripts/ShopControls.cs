using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopControls : MonoBehaviour
{
    public static ShopControls Instance {get;set;}
    GameObject shopMenu;
    [SerializeField] GameObject shopButtonPrefab;
    [SerializeField] AudioSource closeShopSFX, openShopSFX, buySFX;
    public bool shopMenuIsOpen = false;

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
    }

    public void OpenShopMenu(Shop shop){
        if(shopMenuIsOpen){
            return;
        }
        openShopSFX.Play();
        PlayerControls.Instance.HideAxe();
        shopMenuIsOpen = true;
        shopMenu = shop.shopMenu;
        shopMenu.SetActive(true);
        MenuControls.Instance.PausePlayer();
        PopulateShopMenu(shop.itemsForSale, shop.shopButtonParent);
    }

    public void CloseShopMenu(){
        closeShopSFX.Play();
        PlayerControls.Instance.ShowAxe();
        shopMenu.SetActive(false);
        shopMenuIsOpen = false;
        MenuControls.Instance.ResumePlayer();
    }
    
    // public void CloseShopMenuDontResume(){
    //     if(shopMenu){
    //         shopMenu.SetActive(false);
    //     }
    //     shopMenuIsOpen = false;
    // }

    public void PopulateShopMenu(List<int> items, Transform shopButtonParent){
        foreach(Transform child in shopButtonParent){
            Destroy(child.gameObject);
        }
        for(int i=0; i<items.Count;i++){
            int _i = items[i];
            GameObject shopButtonGO = Instantiate(shopButtonPrefab, shopButtonParent);
            shopButtonGO.transform.GetChild(0).GetComponent<Image>().sprite = FurnitureControls.Instance.items[items[i]].sprite;
            shopButtonGO.transform.GetChild(1).GetComponent<Text>().text = FurnitureControls.Instance.items[items[i]].cost.ToString();
            float cost = FurnitureControls.Instance.items[items[i]].cost;
            Button shopButton = shopButtonGO.GetComponent<Button>();
            // if its more than you have, disable
            if(cost>GemControls.Instance.gemCount){
                // shopButton.interactable = false;
                shopButtonGO.transform.GetChild(0).GetComponent<Image>().color = new Color(255,255,255,0.3f);
                shopButtonGO.transform.GetChild(3).gameObject.SetActive(true);
            } else {
                shopButton.onClick.AddListener(()=>{
                    buySFX.Play();
                    GemControls.Instance.RemoveGems(cost);
                    FurnitureControls.Instance.AddItem(_i);
                    PopulateShopMenu(items,shopButtonParent);
                });
            }

        }
    }
}
