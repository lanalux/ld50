using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeEvents : MonoBehaviour
{
    public void InSwing(){
        OnAxeCollide.Instance.inSwing=true;
        OnAxeCollide.Instance.CheckForDamage();
    }

    public void NotInSwing(){
        OnAxeCollide.Instance.inSwing=false;
    }
}
