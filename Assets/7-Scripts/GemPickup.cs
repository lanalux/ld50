using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemPickup : MonoBehaviour
{
    public float numOfGems;
    public float timeToRespawn = 3.0f;
    [SerializeField] OreDeposit fromDeposit;

    void OnTriggerEnter(Collider col){
        if(col.gameObject.layer == LayerMask.NameToLayer("Player")){
            TurnOff();
            GemControls.Instance.AddGems(numOfGems);
            
        }
    }

    public void TurnOff(){
        foreach(Transform child in this.transform.parent){
            Collider col = child.GetComponent<Collider>();
            if(col!=null){
                col.enabled=false;
            } else {
                child.gameObject.SetActive(false);
            }
            
        }
        if(fromDeposit){
            StartCoroutine(fromDeposit.Regen());
        } else {
            StartCoroutine(WaitToRespawn());
        }
    }

    public void TurnOn(){
        GemControls.Instance.PlayRespawnSound();
        foreach(Transform child in this.transform.parent){
            Collider col = child.GetComponent<Collider>();
            if(col!=null){
                col.enabled=true;
            } else {
                child.gameObject.SetActive(true);
            }
        }
    }

    IEnumerator WaitToRespawn(){
        yield return new WaitForSeconds(timeToRespawn);
        TurnOn();
    }
}
