using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleControls : MonoBehaviour
{

    [SerializeField] CanvasGroup overlay;
    [SerializeField] AudioSource startSFX, music;
    [SerializeField] GameObject startGameButton,quitGameButton;
    bool gameIsStarting=false;

    float fadeInTime = 3.0f;
    float fadeOutTime = 2.0f;

    bool fadingIn = true;

    float timePassed;
    float timePassedFadeOut;

    void Start(){
        
        if(PlayerPrefs.HasKey("canPlay")){
            startGameButton.SetActive(false);
            quitGameButton.SetActive(true);
        } else {
            startGameButton.SetActive(true);
            quitGameButton.SetActive(false);
        }
    }

    public void StartGame(){
        startSFX.Play();
        if(gameIsStarting){
            return;
        }
        
        gameIsStarting=true;
        
        PlayerPrefs.SetInt("canPlay", 1);

        PlayerPrefs.Save();
    }


    void Update(){
        if(timePassed < fadeInTime){
            float overlayAlpha = Mathf.Lerp(1.0f, 0, timePassed/fadeInTime);
            timePassed += Time.deltaTime;
            overlay.alpha = overlayAlpha;
        }

        if(!gameIsStarting){
            return;
        }
        
        if(timePassedFadeOut < fadeOutTime){
            float overlayAlpha = Mathf.Lerp(0f, 1.0f, timePassedFadeOut/fadeOutTime);
            
            float soundlerp = Mathf.Lerp(1f, 0.0f, timePassedFadeOut/fadeOutTime);
            timePassedFadeOut += Time.deltaTime;
            music.volume = soundlerp;
            overlay.alpha = overlayAlpha;
        } else {
            SceneManager.LoadScene("Main");
        }
    }

    public void QuitGame(){
        Application.Quit();
    }
}
