using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    public static PlayerControls Instance {get;set;}

    [SerializeField] Animator axeAnim;
    [SerializeField] GameObject axeGO;

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
    }

    void Update(){
        if(Input.GetMouseButtonDown(0)){
            SwingAxe();
        }
    }
   public void SwingAxe(){
       axeAnim.SetTrigger("Swing");
   }

   
    public void HideAxe(){
        axeGO.SetActive(false);
    }

    public void ShowAxe(){
        axeGO.SetActive(true);
    }

}
