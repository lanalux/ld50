using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class EventCollider : MonoBehaviour
{
    bool hasPlayedEvent = false;
    [SerializeField] GameObject charPrefab, speechBubble;
    [SerializeField] Transform pos;
    [SerializeField] CinemachineVirtualCamera vcam;
    [SerializeField] Text textArea;
    [SerializeField] HelperConvo convo;

    void OnTriggerEnter(Collider col){
        if(hasPlayedEvent){
            return;
        }
        if(col.gameObject.layer == LayerMask.NameToLayer("Player")){
            hasPlayedEvent=true;
            HelperControls.Instance.PopIntoPlace(pos, convo, charPrefab, vcam, textArea, speechBubble);
        }
    }
}
